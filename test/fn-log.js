var assert = require('assert');
var sinon = require('sinon');
var through = require('through2');
var express = require('express')
var fnLog = require('..');
var request = require('supertest');
var bodyParser = require('body-parser');

require('buffer');

// Create stream for testing
function st(end) {
    return through(function (chunk, enc, next) {
        if(this.content)
            this.content = Buffer.concat([this.content, chunk]);
        else 
            this.content = chunk;
        next();
    }, end);
}

describe('fnLog()', function() {
    describe('When directly logging', function () {
        it('should log with provided name, version and log data', function () {
            var output = st();
            var log = fnLog('name', 'version', { stream: output });
            var logger = log.logger;
            logger.info({ some: 'data' }, 'message');

            var json = JSON.parse(output.content.toString());
            assert.equal(json.name, 'name');
            assert.equal(json.version, 'version');
            assert.equal(json.some, 'data');
            assert.equal(json.msg, 'message');
        });
    });

    describe('When directly logging with multiple streams', function () {
        it('should log with provided name, version and log data', function () {
            var output = st();
            var log = fnLog('name', 'version', { streams: [ { stream: output } ] });
            var logger = log.logger;
            logger.info({ some: 'data' }, 'message');

            var json = JSON.parse(output.content.toString());
            assert.equal(json.name, 'name');
            assert.equal(json.version, 'version');
            assert.equal(json.some, 'data');
            assert.equal(json.msg, 'message');
        });
    });
    
    describe('When makeing an HTTP request', function () {
        it('should log with provided name, version and HTTP data.', function (done) {
            var app = express();
            var output = st();
            var log = fnLog('name', 'version', { stream: output });
            
            app.use(log.httpLog());

            app.get('/', function (req, res) {
                res.send('hello, world!')
            });

            request(app)
                .get('/')
                .end(function(err, res) {
                    var json = JSON.parse(output.content.toString());
                    assert.equal(json.name, 'name');
                    assert.equal(json.version, 'version');
                    assert.ok(json.res);
                    assert.equal(json.res.statusCode, 200);
                    assert.ok(json.req);
                    assert.ok(json.response_time > 0);
                    assert.equal(json.level, 30);
                    done();
                });
        });
    });

    describe('When making an HTTP request with an error', function () {
        it('should log with provided name, version, HTTP data, request body and error trace.', function (done) {
            var app = express();
            var output = st();
            var log = fnLog('name', 'version', { stream: output });
            
            app.use(log.httpLog({ exclude: [ '/exclude' ] }));

            app.post('/post', bodyParser.json(), function(req, res, next) {
                return next(new Error('Some error'));
            });

            app.use(log.errorLog());
            app.use(function(err, req, res, next) {
                // ignore errors for testing
                next();
            });

            request(app)
                .post('/post')
                .send({ some: 'data' })
                .end(function(err, res) {
                    var json = JSON.parse(output.content.toString());
                    assert.equal(json.name, 'name');
                    assert.equal(json.version, 'version');
                    assert.ok(json.res);
                    assert.ok(json.req);
                    assert.ok(json.response_time > 0);
                    assert.equal(json.level, 50);
                    assert.ok(json.err);
                    assert.equal(json.err.message, 'Some error');
                    assert.deepEqual(json.reqBody, { some: 'data' });

                    done();
                });
        });
    });

    describe('When makeing an HTTP request to an excluded path', function () {
        it('should log with provided name, version and HTTP data.', function (done) {
            var app = express();
            var output = st();
            var log = fnLog('name', 'version', { stream: output });
            
            app.use(log.httpLog({ exclude: [ '/exclude' ] }));

            app.get('/exclude', function (req, res) {
                res.send('hello, world!')
            });

            request(app)
                .get('/exclude')
                .end(function(err, res) {
                    assert.equal(output.content, null);
                    done();
                });
        });
    });
});
