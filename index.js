/**
 * Module exports
 */
module.exports = fnLog;

/**
 * Module dependencies.
 */
var bunyan = require('bunyan');
var onHeaders = require('on-headers');
var onFinished = require('on-finished');

/**
 * Create fnLog object.
 *
 * @param {String} name
 * @param {String} version
 * @param {Object} [options]
 * @return {Object} fnLog
 */
function fnLog(name, version, options) {
    var opts = options || {};

    var logger = bunyan.createLogger({
        name: name,
        version: version,
        serializers: bunyan.stdSerializers,
        stream: opts.stream,
        streams: opts.streams
    });
    
    /**
     * Create the logger middleware.
     *
     * @param {Object} [options]
     * @return {Function} middleware
     */
    var httpLog = function(options) {
        var opts = options || {};
        return function(req, res, next) {
            if (opts.exclude && opts.exclude instanceof Array && opts.exclude.indexOf(req.path) > -1) {
                // Skip logging this
                return next();
            }
            // request data
            req._startAt = process.hrtime();
             
            // response data
            res._startAt = undefined;
            res._err = undefined;

            var responseTime = function() {
                if (!req._startAt || !res._startAt) {
                    // missing request and/or response start time    
                    return;
                } 

                // calculate diff
                var ms = (res._startAt[0] - req._startAt[0]) * 1e3 + (res._startAt[1] - req._startAt[1]) * 1e-6;
                
                // return truncated value
                return ms.toFixed(3);
            };

            var logRequest = function() {
                var logData = {
                    req: req,
                    res: res,
                    err: res._err,
                    response_time: responseTime()
                };

                if (res._err) {
                    logData.reqBody = req.body;
                    logger.error(logData, res._err.message);
                } else {
                    logger.info(logData);
                }
            };
            
            onHeaders(res, function() {
                res._startAt = process.hrtime();
            });

            onFinished(res, logRequest);
            next();
        };
    };
    
    /**
     * Create the error logger middleware.
     *
     * @param {Object} [options]
     * @return {Function} middleware
     */
    var errorLog = function(options) {
        var opts = options || {};
        return function(err, req, res, next) {
            res._err = err;
            next(err);
        };
    };

    return {
        logger: logger,
        httpLog: httpLog,
        errorLog: errorLog    
    };
}

