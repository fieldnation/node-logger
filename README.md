# Node Logger
Node Logger is a JSON logging and HTTP logging middleware for Node.js powered by 
[Bunyan](https://github.com/trentm/node-bunyan) and ruthlessly stolen from [Morgan](https://github.com/expressjs/morgan).

It will log HTTP requests, repsonse-times and record errors with stack traces.

## Install
`npm install`

## API

```js
var nodeLogger = require('node-logger');
```

### nodeLogger(name, version, options)

Create a new log by specifying a `name` and `version` to attach to every log. This will
typically be the package name and package version, respectively.

```js
var packageJson = require('./package.json');

var logger = nodeLogger(packageJson.name, packageJson.version);
```
#### Options
##### stream

Specify a different stream to use with Bunyan. See the
[Bunyan streams documentation](https://github.com/trentm/node-bunyan#streams).
##### streams

Specify a different set of streams to use with Bunyan. See the
[Bunyan streams documentation](https://github.com/trentm/node-bunyan#streams).

### log.logger

Direct access to the Bunyan logger.

```js
var log = logger.log;

log.info({ some: 'information' }, 'some message');
```

### log.httpLog()

HTTP log middelware. Will log all HTTP requests.

#### Options
##### exclude 

An array of paths to exlucde from log.

### log.errorLog()

HTTP error log middelware. Will attach errors and stack trackes to HTTP logs.

## Examples

### express/connect

Simple app that will log all requests and errors to STDOUT

```js
var express = require('express')
var nodeLogger = require('node-logger');

var logger = nodeLogger('simple', 'v0.0.1');

var app = express()

app.use(logger.httpLog());

app.get('/', function (req, res) {
  res.send('hello, world!')
})

app.use(logger.errorLog());
```

## Pretty Output

You can convert the JSON output to human readable text during development by piping your app through Bunyan.
For exapmle, if you have an Expressjs app with a main file of app.js, do the following.

```
node app.js | bunyan
```

## Testing

Test are written using [Mocha](https://mochajs.org) and can be run with the following:

```
npm test
```

Generate coverage report in the `coverage` directory:
```
npm run test-cov
```
